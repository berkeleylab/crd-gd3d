CXX = g++
CPPFLAGS = -O3 -g

AR = ar
ARFLAGS = cru

CUDA_DIR = /usr/local/cuda

INCLUDES = -I$(PWD) -I$(CUDA_DIR)/include
CPPFLAGS = $(INCLUDES)

NVCC = nvcc
NCFLAGS = $(INCLUDES) -arch sm_20 -Xptxas -dlcm=cg

LDFLAGS = -g
LIBS = -L$(CUDA_DIR)/lib -lcudart -lm

HEADERS=$(shell ls *.h)
TARGETS=libgd3d.a sweep/aniso sweep/bilat sweep/nlm tune/aniso tune/bilat tune/nlm

# --- targets
.phony: all clean

all:	$(TARGETS)

%.o:	%.c
	$(CXX) $(CPPFLAGS) -c $< -o $@

%.o:	%.cu
	$(NVCC) $(NCFLAGS) -c $< -o $@

libgd3d.a: ssim.o image.o cutils.o utils.o nifti1_io.o znzlib.o $(HEADERS)
	$(AR) $(ARFLAGS) $@ $^

sweep/aniso:	sweep/aniso.o libgd3d.a
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

sweep/bilat:	sweep/bilat.o libgd3d.a
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

sweep/nlm:	sweep/nlm.o libgd3d.a
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

tune/aniso:	tune/aniso.o libgd3d.a
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

tune/bilat:	tune/bilat.o libgd3d.a
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

tune/nlm:	tune/nlm.o libgd3d.a
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

# --- remove binary and executable files
clean:
	rm -f $(TARGETS) *.o sweep/*.o tune/*.o

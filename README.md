GPU-Accelerating Denoising in 3D (GD3D) is an open-source implementation in
CUDA of three commonly used image denoising methods: bilateral filtering,
anisotropic diffusion, and non-local means. It was written to study the
performance characteristics of these methods on 3D magnetic resonance images,
as described in the paper:

Howison M and Bethel EW. GPU-accelerated denoising of 3D magnetic resonance
images. (under review)

# Installation

[Download](https://bitbucket.org/berkeleylab/crd-gd3d/downloads) the latest
version from Bitbucket. The only prerequisite is the
[CUDA](https://developer.nvidia.com/cuda-downloads) Toolkit 5.0.35. You may
need to adjust the variable `CUDA_DIR` in the `Makefile` to point to your CUDA
installation. Also, you may need to change the line:

    LIBS = -L$(CUDA_DIR)/lib -lcudart -lm

to

    LIBS = -L$(CUDA_DIR)/lib64 -lcudart -lm

to build on a 64-bit Linux system (the included line should work on OS X).

The `Makefile` will build six executables:

    sweep/aniso
    sweep/bilat
    sweep/nlm
    tune/aniso
    tune/bilat
    tune/nlm

The `sweep` executables perform a parameter sweep to determine optimal
*smoothing* parameters, while the `tune` exceutables perform an auto-tuning
study to deterermine optimal memory blocking (see the paper for details).

# License

GD3D is distributed under a modified BSD license. See
[LICENSE](https://bitbucket.org/berkeleylab/crd-gd3d/src/master/LICENSE) for
full details.


/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#include <stdlib.h>
#include <iostream>
#include "cutils.h"

using namespace std;

namespace cutils {

int divide_up(int a, int b)
{
	return (a%b != 0) ? (a/b)+1 : (a/b);
}

void set_constant(void* symbol, void* values, size_t size)
{
	CUCALL(cudaMemcpyToSymbol(symbol, values, size));
}

cudaPitchedPtr alloc3d(size_t size, size_t X, size_t Y, size_t Z)
{
	cudaPitchedPtr ptr;
	cudaExtent vol = make_cudaExtent(size*X, Y, Z);
	CUCALL(cudaMalloc3D(&ptr, vol));
	CUCALL(cudaMemset(ptr.ptr, 0x00, ptr.pitch*Y*Z));
	return ptr;
}

void free3d(cudaPitchedPtr* ptr)
{
	CUCALL(cudaFree(ptr->ptr));
}

void memcpy3d_to(
	void* cpu_ptr, cudaPitchedPtr gpu_ptr, size_t size,
	unsigned X, unsigned Y, unsigned Z, unsigned R)
{
	cudaMemcpy3DParms params = {0};
	params.srcPtr = make_cudaPitchedPtr(cpu_ptr, size*X, X, Y);
	params.srcPos = make_cudaPos(0, 0, 0);
	params.dstPtr = gpu_ptr;
	params.dstPos = make_cudaPos(R*sizeof(float), R, R);
	params.extent = make_cudaExtent(size*X, Y, Z);
	params.kind = cudaMemcpyHostToDevice;
	CUCALL(cudaMemcpy3D(&params));
}

void memcpy3d_from(
	cudaPitchedPtr gpu_ptr, void* cpu_ptr, size_t size,
	unsigned X, unsigned Y, unsigned Z, unsigned R)
{
	cudaMemcpy3DParms params = {0};
	params.srcPtr = gpu_ptr;
	params.srcPos = make_cudaPos(R*sizeof(float), R, R);
	params.dstPtr = make_cudaPitchedPtr(cpu_ptr, size*X, X, Y);
	params.dstPos = make_cudaPos(0, 0, 0);
	params.extent = make_cudaExtent(size*X, Y, Z);
	params.kind = cudaMemcpyDeviceToHost;
	CUCALL(cudaMemcpy3D(&params));
}

} // namespace

// vim: ts=4 sw=4 syntax=cpp noexpandtab

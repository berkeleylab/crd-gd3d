/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#ifndef __GD3D_CUTILS__
#define __GD3D_CUTILS__

#include <stdio.h>
#include <cuda_runtime.h>

#define CUCALL(call) do{\
	cudaError_t err = call ;\
	if (cudaSuccess != err){\
		fprintf(stderr, "cuda error at %s:%d, %s\n",\
				__FILE__, __LINE__, cudaGetErrorString(err));\
		fflush(stderr);\
		exit(1);\
	}\
}while(0)

#define CUCALL_RETURN(call, status) do{\
	cudaError_t err = call ;\
	if (cudaSuccess != err){\
		fprintf(stderr, "cuda error at %s:%d, %s\n",\
				__FILE__, __LINE__, cudaGetErrorString(err));\
		fflush(stderr);\
		status = 0;\
	}\
	else status = 1;\
}while(0)

#define CULAUNCH(kernel,block,grid,args) do{\
	kernel<<<block,grid>>>args ;\
	CUCALL(cudaThreadSynchronize());\
	CUCALL(cudaGetLastError());\
}while(0)

#define CULAUNCH_SHMEM(kernel,block,grid,shmem,args) do{\
	kernel<<<block,grid,shmem>>>args ;\
	CUCALL(cudaThreadSynchronize());\
	CUCALL(cudaGetLastError());\
}while(0)

namespace cutils {

int divide_up(int a, int b);
void set_constant(void* symbol, void* values, size_t size);
cudaPitchedPtr alloc3d(size_t size, size_t X, size_t Y, size_t Z);
void free3d(cudaPitchedPtr* ptr);
void memcpy3d_to(
	void* cpu_ptr, cudaPitchedPtr gpu_ptr, size_t size,
	unsigned X, unsigned Y, unsigned Z, unsigned R);
void memcpy3d_from(
	cudaPitchedPtr gpu_ptr, void* cpu_ptr, size_t size,
	unsigned X, unsigned Y, unsigned Z, unsigned R);

} // namespace

#endif

// vim: ts=4 sw=4 syntax=cpp noexpandtab

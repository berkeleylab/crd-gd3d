/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#include <math.h>
#include "image.h"
#include "nifti1_io.h"

#define NIFTI_READ_DATA 1

Image::Image(const char* filename)
{
	read_nifti(filename);
	normalize();
	ptr = NULL;
}

Image::Image(unsigned dims[3])
{
	this->dims[0] = dims[0];
	this->dims[1] = dims[1];
	this->dims[2] = dims[2];
	nvoxels = dims[0]*dims[1]*dims[2];
	data = new float[nvoxels];
	for (unsigned i=0; i<nvoxels; i++) data[i] = 0.F;
	ptr = NULL;
}

Image::Image(unsigned X, unsigned Y, unsigned Z)
{
	dims[0] = X;
	dims[1] = Y;
	dims[2] = Z;
	nvoxels = X*Y*Z;
	data = new float[nvoxels];
	for (unsigned i=0; i<nvoxels; i++) data[i] = 0.F;
	ptr = NULL;
}

Image::Image(Image& img)
{
	dims[0] = img.dims[0];
	dims[1] = img.dims[1];
	dims[2] = img.dims[2];
	nvoxels = dims[0]*dims[1]*dims[2];
	data = new float[nvoxels];
	for (unsigned i=0; i<nvoxels; i++) data[i] = img.data[i];
	ptr = NULL;
}

Image::~Image()
{
	delete data;
	if (ptr != NULL) cutils::free3d(&cuda_ptr);
}

float* Image::pad(unsigned padding)
{
	const unsigned P = 2*padding;
	const unsigned X = P+dims[0];
	const unsigned Y = P+dims[1];
	const unsigned Z = P+dims[2];
	float* padded = new float[X*Y*Z];
	unsigned indx = 0;
	for (unsigned k=P; k<Z; k++)
		for (unsigned j=P; j<Y; j++)
			for (unsigned i=P; i<X; i++)
				padded[i + j*X + k*X*Y] = data[indx++];
	return padded;
}

void Image::square()
{
	for (unsigned i=0; i<nvoxels; i++) data[i] *= data[i];
}

void Image::multiply(Image& img)
{
	for (unsigned i=0; i<nvoxels; i++) data[i] *= img.data[i];
}

void Image::alloc_gpu(unsigned padding)
{
	const unsigned P = 2*padding;
	if (ptr != NULL) cutils::free3d(&cuda_ptr);
	cuda_ptr = cutils::alloc3d(sizeof(float), P+dims[0], P+dims[1], P+dims[2]);
	ptr = (float*)cuda_ptr.ptr;
	pitch = cuda_ptr.pitch / sizeof(float);
}

void Image::to_gpu(unsigned padding)
{
	cutils::memcpy3d_to(
		(void*)data, cuda_ptr, sizeof(float),
		dims[0], dims[1], dims[2], padding);
}

void Image::from_gpu(unsigned padding)
{
	cutils::memcpy3d_from(
		cuda_ptr, (void*)data, sizeof(float),
		dims[0], dims[1], dims[2], padding);
}

double Image::mse(Image& ref)
{
	register double e;
	register double mse = 0.0;
	for (unsigned i=0; i<nvoxels; i++)
	{
		//e = 255.0*(data[i] - ref.data[i]);
		e = (data[i] - ref.data[i]);
		mse += e*e;
	}
	return mse / (double)nvoxels;
}

double Image::psnr(Image& ref)
{
	register double e;
	register double mse = 0.0;
	register double signal = 0.0;
	for (unsigned i=0; i<nvoxels; i++)
	{
		e = data[i] - ref.data[i];
		mse += e*e;
		signal += data[i]*data[i];
	}
	return 10.0 * log10(signal / mse / (double)nvoxels);
}

void Image::read_nifti(const char* filename)
{
    nifti_image *img = NULL;
    img = nifti_image_read(filename, NIFTI_READ_DATA);
    if (img == NULL) {
	    fprintf(stderr,"Error in nifti_image_read\n");
	    exit(1);
    }
	nvoxels = img->nvox;
	dims[0] = img->nx;
	dims[1] = img->ny;
	dims[2] = img->nz;
	fprintf(stderr, "NIFTI image [%u,%u,%u]\n", dims[0], dims[1], dims[2]);
	data = new float[nvoxels];
    if (img->datatype == DT_FLOAT) {
        fprintf(stderr, " found DT_FLOAT data\n");
		float *f = (float *)img->data;
		for (unsigned i=0; i<nvoxels; i++) {
			data[i] = (float)f[i];
		}
    } else if (img->datatype == DT_UNSIGNED_CHAR) {
        fprintf(stderr, " found DT_UNSIGNED_CHAR data: converting...\n");
        /* convert 8-bit to float */
        unsigned char *c = (unsigned char *)img->data;
        const float norm = 1.F / 255.F;
        for (unsigned i=0; i<nvoxels; i++) {
            data[i] = (float)c[i] * norm;
        }
    } else if (img->datatype == DT_SIGNED_SHORT) {
        fprintf(stderr, " found DT_SIGNED_SHORT data: converting...\n");
        /* convert int16 to float */
        short *d = (short*)img->data;
        for (unsigned i=0; i<nvoxels; i++) {
            data[i] = (float)d[i];
        }
    } else {
        fprintf(stderr, "FATAL: unsupported datatype in %s\n", img->fname);
        exit(EXIT_FAILURE);
    }
	nifti_image_free(img);
}

void Image::write_nifti(char* filename)
{
	int dims[4] = {3, this->dims[0], this->dims[1], this->dims[2]};
	nifti_image *img = nifti_make_new_nim(dims, DT_FLOAT, 1); /* 1 to allocate data */
	float *f = (float *)img->data;
	for (unsigned i=0; i<nvoxels; i++) {
		f[i] = 4095.F * data[i];
	}
	img->fname = filename;
	img->nifti_type = 1; /* NIFTI single file */
	nifti_image_write(img);
	nifti_image_free(img);
}

void Image::normalize()
{
	float min = data[0];
	float max = data[0];
	for (unsigned i=1; i<nvoxels; i++) {
		if (data[i] < min) min = data[i];
		else if (data[i] > max) max = data[i];
	}
	fprintf(stderr," min = %f\n", min);
	fprintf(stderr," max = %f\n", max);
	float scale = 1.0 / (max - min);
	for (unsigned i=0; i<nvoxels; i++) {
		data[i] -= min;
		data[i] *= scale;
	}
}

// vim: ts=4 sw=4 syntax=cpp noexpandtab

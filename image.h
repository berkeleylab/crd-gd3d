/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#ifndef __GD3D_IMAGE__
#define __GD3D_IMAGE__

#include "cutils.h"

class Image
{
	public:
		unsigned nvoxels;
		unsigned dims[3];
		float* data;
		size_t pitch;
		float* ptr;
		float* pad(unsigned padding);
		void square();
		void multiply(Image& img);
		void alloc_gpu(unsigned padding);
		void to_gpu(unsigned padding);
		void from_gpu(unsigned padding);
		double mse(Image& ref);
		double psnr(Image& ref);
		void write_nifti(char* filename);
		Image(const char* filename);
		Image(unsigned X, unsigned Y, unsigned Z);
		Image(unsigned dims[3]);
		Image(Image& img);
		~Image();

	protected:
		cudaPitchedPtr cuda_ptr;
		void read_nifti(const char* filename);
		void normalize();
};

#endif


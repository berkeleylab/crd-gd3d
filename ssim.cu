/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#include "ssim.h"
#include "utils.h"

#define BX 8
#define BY 8

static __constant__ float _weights[11*11*11];

__global__ void smooth(
	const float* src, float* dst,
	unsigned pitch, unsigned X, unsigned Y, unsigned Z, unsigned R)
{
	const unsigned x = threadIdx.x + blockIdx.x * blockDim.x;
	const unsigned y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x >= X || y >= Y) return;

	const unsigned xy = x + y*pitch;
	const unsigned slab = pitch * (Y + 2*R);
	const unsigned pad = R*(1 + pitch + slab);
	const unsigned R1 = 2*R + 1;
	const unsigned R2 = R1*R1;

	for (int z = 0; z < Z; z++)
	{
		const unsigned start = xy + z*slab;

		float sum = 0.F;

		for (unsigned dz = 0; dz <= 2*R; dz++)
		for (unsigned dy = 0; dy <= 2*R; dy++)
		for (unsigned dx = 0; dx <= 2*R; dx++)
			sum += src[start + dx + dy*pitch + dz*slab] *
			      _weights[dx + dy*R1 + dz*R2];

		dst[start + pad] = sum;
	}
}

__global__ void multiply(
	const float* A, const float* B, float* dst, unsigned Z)
{
	const unsigned start = threadIdx.x + blockDim.x * blockIdx.y;
	const unsigned slab = blockDim.x * gridDim.y;
	for (unsigned z=0; z<Z; z++) {
		const unsigned i = start + z*slab;
		dst[i] = A[i]*B[i];
	}
}

__global__ void multiply_sub(
	const float* A, const float* B1, const float* B2, float* dst, unsigned Z)
{
	const unsigned start = threadIdx.x + blockDim.x * blockIdx.y;
	const unsigned slab = blockDim.x * gridDim.y;
	for (unsigned z=0; z<Z; z++) {
		const unsigned i = start + z*slab;
		dst[i] = A[i] - B1[i]*B2[i];
	}
}

__global__ void ssim(
	const float* mu1, const float* mu2,
	const float* sigma1, const float* sigma2, const float* sigma12,
	float* dst, unsigned Z, float c1, float c2)
{
	const unsigned start = threadIdx.x + blockDim.x * blockIdx.y;
	const unsigned slab = blockDim.x * gridDim.y;
	for (unsigned z=0; z<Z; z++) {
		const unsigned i = start + z*slab;
		dst[i] =
			(2.F * mu1[i] * mu2[i] + c1) * (2.F * sigma12[i] + c2) /
			((sigma1[i] + sigma2[i] + c1) * (mu1[i]*mu1[i] + mu2[i]*mu2[i] + c2));
	}
}

void ssim(Image& img1, Image& img2, Image& dst)
{
	const float c1 = 0.01F * 0.01F;
	const float c2 = 0.03F * 0.03F;

	float* weights = utils::gaussian3d(11, 0.5, 1.5);
	cutils::set_constant(_weights, weights, 11*11*11*sizeof(float));
	delete weights;

	img1.alloc_gpu(5);
	img2.alloc_gpu(5);

	img1.to_gpu(5);
	img2.to_gpu(5);

	Image img1sq(img1);
	Image img2sq(img2);

	img1sq.square();
	img2sq.square();

	img1sq.alloc_gpu(5);
	img2sq.alloc_gpu(5);

	img1sq.to_gpu(5);
	img2sq.to_gpu(5);

	Image img12(img1);
	img12.multiply(img2);

	img12.alloc_gpu(5);

	img12.to_gpu(5);

	const unsigned pitch = img1.pitch;
	const unsigned X = img1.dims[0];
	const unsigned Y = img1.dims[1];
	const unsigned Z = img1.dims[2];

	const unsigned PY = Y + 10;
	const unsigned PZ = Z + 10;

	cudaPitchedPtr _mu1 = cutils::alloc3d(sizeof(float), pitch, PY, PZ);
	cudaPitchedPtr _mu2 = cutils::alloc3d(sizeof(float), pitch, PY, PZ);
	cudaPitchedPtr _sigma1 = cutils::alloc3d(sizeof(float), pitch, PY, PZ);
	cudaPitchedPtr _sigma2 = cutils::alloc3d(sizeof(float), pitch, PY, PZ);
	cudaPitchedPtr _sigma12 = cutils::alloc3d(sizeof(float), pitch, PY, PZ);

	float* mu1 = (float*)_mu1.ptr;
	float* mu2 = (float*)_mu2.ptr;
	float* sigma1 = (float*)_sigma1.ptr;
	float* sigma2 = (float*)_sigma2.ptr;
	float* sigma12 = (float*)_sigma12.ptr;

	dim3 block3(BX, BY, 1);
	dim3 grid3((X-1)/BX+1, (Y-1)/BY+1, 1);

	dim3 block1(pitch, 1, 1);
	dim3 grid1(1, PY, 1);

	CULAUNCH(smooth, grid3, block3, (img1.ptr, mu1, pitch, X, Y, Z, 5));
	CULAUNCH(smooth, grid3, block3, (img2.ptr, mu2, pitch, X, Y, Z, 5));

	CULAUNCH(smooth, grid3, block3, (img1sq.ptr, sigma1, pitch, X, Y, Z, 5));
	CULAUNCH(multiply_sub, grid1, block1, (sigma1, mu1, mu1, sigma1, PZ));

	CULAUNCH(smooth, grid3, block3, (img2sq.ptr, sigma2, pitch, X, Y, Z, 5));
	CULAUNCH(multiply_sub, grid1, block1, (sigma2, mu2, mu2, sigma2, PZ));

	CULAUNCH(smooth, grid3, block3, (img12.ptr, sigma12, pitch, X, Y, Z, 5));
	CULAUNCH(multiply_sub, grid1, block1, (sigma12, mu1, mu2, sigma12, PZ));

	dst.alloc_gpu(5);
	CULAUNCH(
		ssim, grid1, block1,
		(mu1, mu2, sigma1, sigma2, sigma12, dst.ptr, PZ, c1, c2));
	dst.from_gpu(5);

	cutils::free3d(&_mu1);
	cutils::free3d(&_mu2);
	cutils::free3d(&_sigma1);
	cutils::free3d(&_sigma2);
	cutils::free3d(&_sigma12);
}

double mssim(Image& img1, Image& img2)
{
	double mssim = 0;
	Image tmp(img1.dims);
	ssim(img1, img2, tmp);
	for (int i=0; i<tmp.nvoxels; i++) mssim += (double)tmp.data[i];
	return mssim / (double)(tmp.nvoxels);
}

// vim: ts=4 sw=4 syntax=cpp noexpandtab

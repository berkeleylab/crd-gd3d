/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#include <getopt.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

#define PROGNAME "sweep/aniso"

#include "utils.h"
#include "cutils.h"
#include "image.h"
#include "ssim.h"

#define BX 8
#define BY 8

static __constant__ float diagonals[4];

using namespace std;

__global__ void aniso(
	const float *src, float *dst,
	unsigned pitch, unsigned X, unsigned Y, unsigned Z,
	float DT, float K)
{
	const unsigned x = threadIdx.x + blockIdx.x * blockDim.x;
	const unsigned y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x >= X || y >= Y) return;

	const unsigned xy = x + y*pitch;
	const unsigned slab = pitch * (Y + 2);
	const unsigned pad = 1 + pitch + slab;

	for (unsigned z = 0; z < Z; z++)
	{
		const int start = xy + z*slab + pad;

        float v0 = src[start];
        float v1 = 0.F;

        for (int dz=-1; dz<=1; dz++){
		for (int dy=-1; dy<=1; dy++){
		for (int dx=-1; dx<=1; dx++)
		{
			/* gradient by central difference */
			float nabla = src[start + dx + dy*pitch + dz*slab] - v0;
			/* conductance function */
			float c = 1.F / (1.F + nabla * nabla * K);
			/* normalize by diagonal lengths from lookup table */
			v1 += c * nabla * diagonals[dx*dx + dy*dy + dz*dz];
        }
        }
        }

        dst[start] = v0 + DT * v1;
	}
}

void print_usage()
{
	cout <<
"usage: aniso -i INPUT -r REFERENCE -n N1[,N2...] -t T1[,T2...] -k K1[,K2...]\n"
"\n"
"    -i  input file in ANALYZE or NIFTI format\n"
"    -r  reference (noiseless) file in ANALYZE or NIFTI format\n"
"\n"
"   Parameters to sweep (specify with comma-separated list):\n"
"    -n  number of iterations\n"
"    -t  timestep\n"
"    -k  scaling factor\n";
	exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
	const char* input = NULL;
	const char* reference = NULL;
	vector<int> ns;
	vector<float> ts;
	vector<float> ks;

	int c;
	while ((c = getopt(argc, argv, "i:r:n:t:k:")) != -1)
		switch (c) {
			case 'i':
				input = optarg;
				break;
			case 'r':
				reference = optarg;
				break;
			case 'n':
				utils::split_int(optarg, ",", ns);
				break;
			case 't':
				utils::split_float(optarg, ",", ts);
				break;
			case 'k':
				utils::split_float(optarg, ",", ks);
				break;
			default:
				print_usage();
		}

	utils::check_arg('i', input != NULL);
	utils::check_arg('r', reference != NULL);
	utils::check_arg('n', ns.size() > 0);
	utils::check_arg('t', ts.size() > 0);
	utils::check_arg('k', ks.size() > 0);

	cudaSetDevice(0);

	Image I(input);
	Image J(I);
	Image R(reference);

	const unsigned X = I.dims[0];
	const unsigned Y = I.dims[1];
	const unsigned Z = I.dims[2];

	dim3 block(BX, BY, 1);
	dim3 grid((X-1)/BX+1, (Y-1)/BY+1, 1);

	/* table for normalizing by diagonal length */
	float d[4];
	d[0] = 0.F;
	d[1] = 1.F;
	d[2] = 1.F / sqrt(2.F);
	d[3] = 1.F / sqrt(3.F);
	cutils::set_constant(diagonals, d, 4*sizeof(float));

	double start, elapsed;

	for (unsigned i=0; i<ns.size(); i++)
	{
		for (unsigned j=0; j<ts.size(); j++)
		{
			for (unsigned k=0; k<ks.size(); k++)
			{
				float kinv2 = 1.0f / ks[k];
				kinv2 *= kinv2;

				Image K(I);

				// Allocate GPU memory and transfer image
				K.alloc_gpu(1);
				J.alloc_gpu(1);
				K.to_gpu(1);

				// Launch filter kernel
				start = utils::timestamp();
				Image* src = &K;
				Image* dst = &J;
				for (unsigned t=0; t<ns[i]; t++) {
					CULAUNCH(
						aniso, grid, block,
						(src->ptr, dst->ptr, src->pitch, X, Y, Z, ts[j], kinv2));
					Image* tmp = dst;
					dst = src;
					src = tmp;
				}
				dst = src;
				elapsed = utils::timestamp() - start;

				// Transfer image back and compute similarity with reference
				dst->from_gpu(1);
				//dst->write_nifti("test.nii");

				cout << ns[i] << '\t' << ts[j] << '\t' << ks[k] << '\t'
					 << I.mse(R) << '\t' << dst->mse(R) << '\t'
					 << I.psnr(R) << '\t' << dst->psnr(R) << '\t'
					 << mssim(I, R) << '\t' << mssim(*dst, R) << '\t'
					 << elapsed << endl;
			}
		}
	}

	return EXIT_SUCCESS;
}

// vim: ts=4 sw=4 syntax=cpp noexpandtab

/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#include <getopt.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

#define PROGNAME "sweep/bilat"

#include "utils.h"
#include "cutils.h"
#include "image.h"
#include "ssim.h"

#define BX 32
#define BY 16

#define MAX_R 11
#define MAX_D (2*MAX_R + 1)

static __constant__ float _domain_weights[MAX_D * MAX_D * MAX_D];
static __constant__ float _range_weights[256];

using namespace std;

__global__ void bilat(
	const float *src, float *dst,
	unsigned pitch, unsigned X, unsigned Y, unsigned Z, unsigned R)
{
	const unsigned x = threadIdx.x + blockIdx.x * blockDim.x;
	const unsigned y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x >= X || y >= Y) return;

	const unsigned xy = x + y*pitch;
	const unsigned slab = pitch * (Y + 2*R);
	const unsigned pad = R*(1 + pitch + slab);
	const unsigned R1 = 2*R + 1;
	const unsigned R2 = R1*R1;

	for (unsigned z = 0; z < Z; z++)
	{
		const unsigned start = xy + z*slab;

		float v0 = src[start + pad];
		float sum = 0.0f;
		float weights = 0.0f;

		for (unsigned dz = 0; dz <= 2*R; dz++){
		for (unsigned dy = 0; dy <= 2*R; dy++){
		for (unsigned dx = 0; dx <= 2*R; dx++)
		{
			float v = src[start + dx + dy*pitch + dz*slab];
			float weight = 
				_domain_weights[dx + dy*R1 + dz*R2] *
				_range_weights[__float2int_rd(fabs(v0-v)*255.0)];

			weights += weight;
			sum += v*weight;
		}
		}
		}

		dst[start + pad] = sum / weights;
	}
}

void print_usage()
{
	cout <<
"usage: bilat -i INPUT -r REFERENCE -R R1[,R2...] -a A1[,A2...] -b B1[,B2...]\n"
"\n"
"    -i  input file in ANALYZE or NIFTI format\n"
"    -r  reference (noiseless) file in ANALYZE or NIFTI format\n"
"\n"
"   Parameters to sweep (specify with comma-separated list):\n"
"    -R  radius\n"
"    -s  range stdev\n"
"    -d  domain stdev\n";
	exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
	const char* input = NULL;
	const char* reference = NULL;
	vector<int> rs;
	vector<float> ss;
	vector<float> ds;

	int c;
	while ((c = getopt(argc, argv, "i:r:R:s:d:")) != -1)
		switch (c) {
			case 'i':
				input = optarg;
				break;
			case 'r':
				reference = optarg;
				break;
			case 'R':
				utils::split_int(optarg, ",", rs);
				break;
			case 's':
				utils::split_float(optarg, ",", ss);
				break;
			case 'd':
				utils::split_float(optarg, ",", ds);
				break;
			default:
				print_usage();
		}

	utils::check_arg('i', input != NULL);
	utils::check_arg('r', reference != NULL);
	utils::check_arg('R', rs.size() > 0);
	utils::check_arg('s', ss.size() > 0);
	utils::check_arg('d', ds.size() > 0);

	for (unsigned i=0; i<rs.size(); i++) {
		if (rs[i] > MAX_R)
			ARG_ERROR("radius can't exceed " << MAX_R);
	}

	cudaSetDevice(0);

	Image I(input);
	Image J(I);
	Image R(reference);

	const unsigned X = I.dims[0];
	const unsigned Y = I.dims[1];
	const unsigned Z = I.dims[2];

	dim3 block(BX, BY, 1);
	dim3 grid((X-1)/BX+1, (Y-1)/BY+1, 1);

	float *weights;
	double start, elapsed;

	for (unsigned i=0; i<rs.size(); i++)
	{
		unsigned r = rs[i];
		const unsigned diam = 2*r + 1;

		for (unsigned j=0; j<ss.size(); j++)
		{
			weights = utils::gaussian1d(256, 0.0, ss[j]);
			cutils::set_constant(_range_weights, weights, 256*sizeof(float));
			delete weights;

			for (unsigned k=0; k<ds.size(); k++)
			{
				weights = utils::gaussian3d(diam, 0.5, ds[k]);
				cutils::set_constant(
					_domain_weights, weights, diam*diam*diam*sizeof(float));
				delete weights;

				// Allocate GPU memory and transfer image
				I.alloc_gpu(r);
				J.alloc_gpu(r);
				I.to_gpu(r);

				// Launch filter kernel
				start = utils::timestamp();
				CULAUNCH(
					bilat, grid, block,
					(I.ptr, J.ptr, I.pitch, X, Y, Z, r));
				elapsed = utils::timestamp() - start;

				// Transfer image back and compute similarity with reference
				J.from_gpu(r);
				cout << r << '\t' << ss[j] << '\t' << ds[k] << '\t'
					 << I.mse(R) << '\t' << J.mse(R) << '\t'
					 << I.psnr(R) << '\t' << J.psnr(R) << '\t'
					 << mssim(I, R) << '\t' << mssim(J, R) << '\t'
				     << elapsed << endl;
			}
		}	
	}

	return EXIT_SUCCESS;
}

// vim: ts=4 sw=4 syntax=cpp noexpandtab

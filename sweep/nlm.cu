/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#include <getopt.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

#define PROGNAME "sweep/nlm"

#include "utils.h"
#include "cutils.h"
#include "image.h"
#include "ssim.h"

#define BX 32
#define BY 16

using namespace std;

__global__ void nlm(
	const float *src, float *dst,
	unsigned pitch, unsigned X, unsigned Y, unsigned Z,
	unsigned W, unsigned P, float H)
{
	const unsigned x = threadIdx.x + blockIdx.x * blockDim.x;
	const unsigned y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x >= X || y >= Y) return;

	const unsigned xy = x + y*pitch;
	const unsigned slab = pitch * (Y + 2*(W+P));
	const unsigned pad = 1 + pitch + slab;

	for (unsigned z = 0; z < Z; z++)
	{
		const unsigned start = xy + z*slab;
		const unsigned wstart = start + P * pad;
		const unsigned pstart = start + W * pad;

		float sum = 0.0f;
		float weights = 0.0f;

		for (unsigned wz = 0; wz <= 2*W; wz++){
		for (unsigned wy = 0; wy <= 2*W; wy++){
		for (unsigned wx = 0; wx <= 2*W; wx++)
		{
			float weight = 0.0f;

			for (unsigned pz = 0; pz <= 2*P; pz++){
			for (unsigned py = 0; py <= 2*P; py++){
			for (unsigned px = 0; px <= 2*P; px++)
			{
				float d = src[pstart + px + py*pitch + pz*slab] -
				          src[start + wx+px + (wy+py)*pitch + (wz+pz)*slab];
				weight += d*d;
			}
			}
			}

			weight = exp( -(weight*weight*H) );

			weights += weight;
			sum += weight * src[wstart + wx + wy*pitch + wz*slab];
		}
		}
		}

		dst[start + (W+P)*pad] = sum / weights;
	}
}

void print_usage()
{
	cout <<
"usage: nlm -i INPUT -r REFERENCE -w W1[,W2...] -p P1[,P2...] -h H1[,H2...]\n"
"\n"
"    -i  input file in ANALYZE or NIFTI format\n"
"    -r  reference (noiseless) file in ANALYZE or NIFTI format\n"
"\n"
"   Parameters to sweep (specify with comma-separated list):\n"
"    -w  window radius\n"
"    -p  patch radius\n"
"    -h  scaling factor\n";
	exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
	const char* input = NULL;
	const char* reference = NULL;
	vector<int> ws;
	vector<int> ps;
	vector<float> hs;

	int c;
	while ((c = getopt(argc, argv, "i:r:w:p:h:")) != -1)
		switch (c) {
			case 'i':
				input = optarg;
				break;
			case 'r':
				reference = optarg;
				break;
			case 'w':
				utils::split_int(optarg, ",", ws);
				break;
			case 'p':
				utils::split_int(optarg, ",", ps);
				break;
			case 'h':
				utils::split_float(optarg, ",", hs);
				break;
			default:
				print_usage();
		}

	utils::check_arg('i', input != NULL);
	utils::check_arg('r', reference != NULL);
	utils::check_arg('w', ws.size() > 0);
	utils::check_arg('p', ps.size() > 0);
	utils::check_arg('h', hs.size() > 0);

	if (ws.size() != ps.size())
		ARG_ERROR("window and patch lists must have same length");

	cudaSetDevice(0);

	Image I(input);
	Image J(I);
	Image R(reference);

	const unsigned X = I.dims[0];
	const unsigned Y = I.dims[1];
	const unsigned Z = I.dims[2];

	dim3 block(BX, BY, 1);
	dim3 grid((X-1)/BX+1, (Y-1)/BY+1, 1);

	double start, elapsed;

	for (unsigned i=0; i<ws.size(); i++)
	{
		int w = ws[i];
		int p = ps[i];

		for (unsigned j=0; j<hs.size(); j++)
		{
			float h = hs[j];
			float hinv2 = 1.0f / h;
			hinv2 *= hinv2;

			// Allocate GPU memory and transfer image
			I.alloc_gpu(w+p);
			J.alloc_gpu(w+p);
			I.to_gpu(w+p);

			// Launch filter kernel
			start = utils::timestamp();
			CULAUNCH(
				nlm, grid, block,
				(I.ptr, J.ptr, I.pitch, X, Y, Z, w, p, hinv2));
			elapsed = utils::timestamp() - start;

			// Transfer image back and compute similarity with reference
			J.from_gpu(w+p);
			cout << w << '\t' << p << '\t' << h << '\t'
				 << I.mse(R) << '\t' << J.mse(R) << '\t'
				 << I.psnr(R) << '\t' << J.psnr(R) << '\t'
			     << mssim(I, R) << '\t' << mssim(J, R) << '\t'
			     << elapsed << endl;
			//J.write_nifti("test.nii");
		}
	}

	return EXIT_SUCCESS;
}

// vim: ts=4 sw=4 syntax=cpp noexpandtab

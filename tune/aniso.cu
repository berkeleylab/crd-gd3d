/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#include <getopt.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

#define PROGNAME "tune/aniso"

#include "utils.h"
#include "cutils.h"
#include "image.h"

#define N 20
#define KP 100.0
#define DT 0.015625

#define MAX_SHMEM (48*1024)

static __constant__ float diagonals[4];

using namespace std;

__global__ void aniso(
	const float *src, float *dst,
	unsigned pitch, unsigned X, unsigned Y, unsigned Z)
{
	extern __shared__ float block[];

	const unsigned x = threadIdx.x + blockIdx.x * blockDim.x;
	const unsigned y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x >= X || y >= Y) return;

	const unsigned xy = x + y*pitch;
	const unsigned slab = pitch * (Y + 2);
	const unsigned pad = 1 + pitch + slab;

	/* the difference of this threads position relative to the block
	 * boundary as a flat index */
	const unsigned dt = threadIdx.x + threadIdx.y*pitch;

	/* "b" variables are in coordinates local to the cache block */
	const unsigned BX = blockDim.x;
	const unsigned BY = blockDim.y;
	const unsigned bpitch = BX + 2;
	const unsigned bslab = bpitch * (BY + 2);
	const unsigned bpad = 1 + bpitch + bslab;

	for (unsigned z = 0; z < Z; z++)
	{
		unsigned start = (xy - dt) + z*slab;

		/* prefetch block with BZ depth */
		__syncthreads();

		for (unsigned bz = 0; bz < 3; bz++)
		{
			/* the first R threads will wrap around and execute these
			 * loops twice */
			for (unsigned by = threadIdx.y; by < (BY+2); by += BY)
			{
				for (unsigned bx = threadIdx.x; bx < (BX+2); bx += BX)
				{
					/* copy from global memory into the shared memory block */
					block[bx + bpitch*by + bslab*bz] =
						src[start + bx + pitch*by + slab*bz];
				}
			}
		}

		__syncthreads();

		/* perform calculations across BZ depth */
		const unsigned bstart = threadIdx.x + threadIdx.y*bpitch + bpad;

		float v0 = block[bstart];
		float v1 = 0.F;

		for (int dz=-1; dz<=1; dz++){
		for (int dy=-1; dy<=1; dy++){
		for (int dx=-1; dx<=1; dx++)
		{
			/* gradient by central difference */
			float nabla = block[bstart + dx + dy*bpitch + dz*bslab] - v0;
			/* conductance function */
			float c = 1.F / (1.F + nabla * nabla * KP);
			/* normalize by diagonal lengths from lookup table */
			v1 += c * nabla * diagonals[dx*dx + dy*dy + dz*dz];
		}
		}
		}

		dst[xy + z*slab + pad] = v0 + DT * v1;
	}
}

void print_usage()
{
	cout <<
"usage: nlm -i INPUT -r REFERENCE -w W1[,W2...] -p P1[,P2...] -h H1[,H2...]\n"
"\n"
"    -i  input file in ANALYZE or NIFTI format\n"
"\n"
"   Blocking parameters to tune (specify with comma-separated list):\n"
"    -x  block size in X dimension\n"
"    -y  block size in Y dimension\n"
"    -z  block size in Z dimension\n";
	exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
	const char* input = NULL;
	vector<int> xs;
	vector<int> ys;

	int c;
	while ((c = getopt(argc, argv, "i:x:y:")) != -1)
		switch (c) {
			case 'i':
				input = optarg;
				break;
			case 'x':
				utils::split_int(optarg, ",", xs);
				break;
			case 'y':
				utils::split_int(optarg, ",", ys);
				break;
			default:
				print_usage();
		}

	utils::check_arg('i', input != NULL);
	utils::check_arg('x', xs.size() > 0);
	utils::check_arg('y', ys.size() > 0);

	cudaSetDevice(0);

	Image I(input);
	Image J(I);

	const unsigned X = I.dims[0];
	const unsigned Y = I.dims[1];
	const unsigned Z = I.dims[2];

	double start, alloc_time, xfer1_time, filter_time, xfer2_time;
	bool status;

	/* table for normalizing by diagonal length */
	float d[4];
	d[0] = 0.F;
	d[1] = 1.F;
	d[2] = 1.F / sqrt(2.F);
	d[3] = 1.F / sqrt(3.F);
	cutils::set_constant(diagonals, d, 4*sizeof(float));

	for (unsigned j=0; j<ys.size(); j++)
	{
		unsigned BY = ys[j];
		if (BY > Y) continue;

		for (unsigned i=0; i<xs.size(); i++)
		{
			unsigned BX = xs[i];
			unsigned shmem = (BX+2)*(BY+2)*3*sizeof(float);

			if (BX > X || BX*BY < 32 || shmem > MAX_SHMEM) continue;

			dim3 block(BX, BY, 1);
			dim3 grid((X-1)/BX+1, (Y-1)/BY+1, 1);

			Image K(I);

			// Allocate GPU memory and transfer image
			start = utils::timestamp();
			K.alloc_gpu(1);
			J.alloc_gpu(1);
			alloc_time = utils::timestamp() - start;

			start = utils::timestamp();
			K.to_gpu(1);
			xfer1_time = utils::timestamp() - start;

			// Launch filter kernel
			start = utils::timestamp();
			Image* src = &K;
			Image* dst = &J;
			for (unsigned t=0; t<N; t++) {
				aniso<<<grid, block, shmem>>>(src->ptr, dst->ptr, src->pitch, X, Y, Z);
				CUCALL(cudaThreadSynchronize());
				CUCALL_RETURN(cudaGetLastError(), status);
				Image* tmp = dst;
				dst = src;
				src = tmp;
			}
			dst = src;
			filter_time = utils::timestamp() - start;

			// Transfer image back and compute similarity with reference
			start = utils::timestamp();
			dst->from_gpu(1);
			xfer2_time = utils::timestamp() - start;

			cout << BX << '\t' << BY << '\t' << status << '\t'
				 << alloc_time << '\t' << xfer1_time << '\t'
				 << filter_time << '\t' << xfer2_time << endl;

			//dst->write_nifti("test.nii");
		}
	}

	return EXIT_SUCCESS;
}

// vim: ts=4 sw=4 syntax=cpp noexpandtab

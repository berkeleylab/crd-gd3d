/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#include <getopt.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

#define PROGNAME "tune/nlm"

#include "utils.h"
#include "cutils.h"
#include "image.h"

#define W 3
#define P 2
#define R (2*(W+P))
#define H 100.0

#define MAX_SHMEM (48*1024)

using namespace std;

__global__ void nlm(
	const float *src, float *dst,
	unsigned pitch, unsigned X, unsigned Y, unsigned Z)
{
	extern __shared__ float block[];

	const unsigned x = threadIdx.x + blockIdx.x * blockDim.x;
	const unsigned y = threadIdx.y + blockIdx.y * blockDim.y;
	if (x >= X || y >= Y) return;

	const unsigned xy = x + y*pitch;
	const unsigned slab = pitch * (Y + R);
	const unsigned pad = R*(1 + pitch + slab);

	/* the difference of this threads position relative to the block
	 * boundary as a flat index */
	const unsigned dt = threadIdx.x + threadIdx.y*pitch;

	/* "b" variables are in coordinates local to the cache block */
	const unsigned BX = blockDim.x;
	const unsigned BY = blockDim.y;
	const unsigned bpitch = BX + R;
	const unsigned bslab = bpitch * (BY + R);
	const unsigned bpad = 1 + bpitch + bslab;

	for (unsigned z = 0; z < Z; z++)
	{
		unsigned start = (xy - dt) + z*slab;

		/* prefetch block with BZ depth */
		__syncthreads();

		for (unsigned bz = 0; bz < (1+R); bz++)
		{
			/* the first R threads will wrap around and execute these
			 * loops twice */
			for (unsigned by = threadIdx.y; by < (BY+R); by += BY)
			{
				for (unsigned bx = threadIdx.x; bx < (BX+R); bx += BX)
				{
					/* copy from global memory into the shared memory block */
					block[bx + bpitch*by + bslab*bz] =
						src[start + bx + pitch*by + slab*bz];
				}
			}
		}

		__syncthreads();

		/* perform calculations across BZ depth */

		const unsigned bstart = threadIdx.x + threadIdx.y*bpitch;
		const unsigned wstart = bstart + P * bpad;
		const unsigned pstart = bstart + W * bpad;

		float sum = 0.0f;
		float weights = 0.0f;

		for (unsigned wz = 0; wz <= 2*W; wz++){
		for (unsigned wy = 0; wy <= 2*W; wy++){
		for (unsigned wx = 0; wx <= 2*W; wx++)
		{
			float weight = 0.0f;

			for (unsigned pz = 0; pz <= 2*P; pz++){
			for (unsigned py = 0; py <= 2*P; py++){
			for (unsigned px = 0; px <= 2*P; px++)
			{
				float d =
					block[pstart + px + py*bpitch + pz*bslab] -
					block[bstart + wx+px + (wy+py)*bpitch + (wz+pz)*bslab];
				weight += d*d;
			}
			}
			}

			weight = exp( -(weight*weight*H) );

			weights += weight;
			sum += weight * block[wstart + wx + wy*bpitch + wz*bslab];
		}
		}
		}

		dst[xy + z*slab + pad] = sum / weights;
	}
}

void print_usage()
{
	cout <<
"usage: nlm -i INPUT -r REFERENCE -w W1[,W2...] -p P1[,P2...] -h H1[,H2...]\n"
"\n"
"    -i  input file in ANALYZE or NIFTI format\n"
"\n"
"   Blocking parameters to tune (specify with comma-separated list):\n"
"    -x  block size in X dimension\n"
"    -y  block size in Y dimension\n"
"    -z  block size in Z dimension\n";
	exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
	const char* input = NULL;
	vector<int> xs;
	vector<int> ys;

	int c;
	while ((c = getopt(argc, argv, "i:x:y:")) != -1)
		switch (c) {
			case 'i':
				input = optarg;
				break;
			case 'x':
				utils::split_int(optarg, ",", xs);
				break;
			case 'y':
				utils::split_int(optarg, ",", ys);
				break;
			default:
				print_usage();
		}

	utils::check_arg('i', input != NULL);
	utils::check_arg('x', xs.size() > 0);
	utils::check_arg('y', ys.size() > 0);

	cudaSetDevice(0);

	Image I(input);
	Image J(I);

	const unsigned X = I.dims[0];
	const unsigned Y = I.dims[1];
	const unsigned Z = I.dims[2];

	double start, alloc_time, xfer1_time, filter_time, xfer2_time;
	bool status;

	for (unsigned j=0; j<ys.size(); j++)
	{
		unsigned BY = ys[j];
		if (BY > Y) continue;

		for (unsigned i=0; i<xs.size(); i++)
		{
			unsigned BX = xs[i];
			unsigned shmem = (BX+R)*(BY+R)*(1+R)*sizeof(float);

			if (BX > X || BX*BY < 32 || shmem > MAX_SHMEM) continue;

			dim3 block(BX, BY, 1);
			dim3 grid((X-1)/BX+1, (Y-1)/BY+1, 1);

			// Allocate GPU memory and transfer image
			start = utils::timestamp();
			I.alloc_gpu(W+P);
			J.alloc_gpu(W+P);
			alloc_time = utils::timestamp() - start;

			start = utils::timestamp();
			I.to_gpu(W+P);
			xfer1_time = utils::timestamp() - start;

			// Launch filter kernel
			start = utils::timestamp();
			nlm<<<grid, block, shmem>>>(I.ptr, J.ptr, I.pitch, X, Y, Z);
			CUCALL(cudaThreadSynchronize());
			CUCALL_RETURN(cudaGetLastError(), status);
			filter_time = utils::timestamp() - start;

			// Transfer image back and compute similarity with reference
			start = utils::timestamp();
			J.from_gpu(W+P);
			xfer2_time = utils::timestamp() - start;

			cout << BX << '\t' << BY << '\t' << status << '\t'
				 << alloc_time << '\t' << xfer1_time << '\t'
				 << filter_time << '\t' << xfer2_time << endl;
		}
	}

	return EXIT_SUCCESS;
}

// vim: ts=4 sw=4 syntax=cpp noexpandtab

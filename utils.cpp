/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <iostream>
#include <vector>

using namespace std;

namespace utils {

double timestamp()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (double)tv.tv_sec + tv.tv_usec * 1e-6;
}

void split_int(char* str, const char* delim, vector<int>& result)
{
	char* token = strtok(str, delim);
	while(token != NULL)
	{
		result.push_back(atoi(token));
		token = strtok(NULL, delim);
	}
}

void split_float(char* str, const char* delim, vector<float>& result)
{
	char* token = strtok(str, delim);
	while(token != NULL)
	{
		result.push_back(atof(token));
		token = strtok(NULL, delim);
	}
}

void check_arg(char c, bool test)
{
	if (!test) {
		cerr << "error: missing argument '-" << c << "'" << endl;
		exit(EXIT_FAILURE);
	}
}

float* gaussian1d(unsigned size, double mean, double stdev)
{
	float *weights = new float[size];

	double scale = 1.0/(stdev * sqrt(2.0 * M_PI));
	double divisor = 1.0 / (2.0 * stdev * stdev);
	double sum = 0.0;

	for (unsigned i=0; i<size; i++)
	{
		double x = (double)i - mean;
		weights[i] = scale * exp(-1.0 * x * x * divisor);
		sum += weights[i];
	}

	double norm = 1.0 / sum;
	for (unsigned i=0; i<size; i++) weights[i] *= norm;

	return weights;
}

float* gaussian3d(unsigned size, double mean, double stdev)
{
	float *weights = new float[size*size*size];

	double sum = 0.0;
	float* w = gaussian1d(size, mean, stdev);

	unsigned indx = 0;
	for (unsigned k=0; k<size; k++) {
		for (unsigned j=0; j<size; j++) {
			for (unsigned i=0; i<size; i++) {
				weights[indx] = w[i] * w[j] * w[k];
				sum += weights[indx];
				indx++;
			}
		}
	}

	delete w;

	double norm = 1.0 / sum;
	indx = 0;
	for (unsigned k=0; k<size; k++) {
		for (unsigned j=0; j<size; j++) {
			for (unsigned i=0; i<size; i++) {
				weights[indx++] *= norm;
			}
		}
	}

	return weights;
}

} // namespace

// vim: ts=4 sw=4 syntax=cpp noexpandtab

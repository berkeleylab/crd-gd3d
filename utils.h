/* GPU-Accelerated Denoising in 3D, Copyright (c) 2013, The Regents of the
 * University of California, through Lawrence Berkeley National Laboratory
 * (subject to receipt of any required approvals from the U.S. Dept. of
 * Energy).  All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Technology Transfer Department at
 * TTD@lbl.gov.
 *
 * NOTICE.  This software is owned by the U.S. Department of Energy.  As such,
 * the U.S. Government has been granted for itself and others acting on its
 * behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, prepare derivative works, and perform publicly and
 * display publicly.  Beginning five (5) years after the date permission to
 * assert copyright is obtained from the U.S. Department of Energy, and subject
 * to any subsequent five (5) year renewals, the U.S. Government is granted for
 * itself and others acting on its behalf a paid-up, nonexclusive, irrevocable,
 * worldwide license in the Software to reproduce, prepare derivative works,
 * distribute copies to the public, perform publicly and display publicly, and
 * to permit others to do so.
 *
 * Author: Mark Howison
 */

#include <vector>

#ifndef __GD3D_UTILS__
#define __GD3D_UTILS__

#define NOTIFY(msg) cerr << PROGNAME": " << msg << endl; cerr.flush();
#define ARG_ERROR(msg) do{ NOTIFY(msg) print_usage(); exit(EXIT_FAILURE); }while(0);

namespace utils {

double timestamp();
void split_int(char* str, const char* delim, std::vector<int>& result);
void split_float(char* str, const char* delim, std::vector<float>& result);
void check_arg(char c, bool test);
float* gaussian1d(unsigned size, double mean, double stdev);
float* gaussian3d(unsigned size, double mean, double stdev);

}

#endif

// vim: ts=4 sw=4 syntax=cpp noexpandtab
